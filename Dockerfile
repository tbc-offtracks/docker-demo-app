FROM tchiotludo/akhq:0.17.0
USER root

RUN apt-get update && apt-get upgrade -y dpkg && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get upgrade -y debian-archive-keyring && rm -rf /var/lib/apt/lists/*

USER akhq
